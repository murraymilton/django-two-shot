from django.forms import ModelForm
from django import forms
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "category",
            "account",
            "date",
        ]
        widgets = {
            "date": forms.DateTimeInput(attrs={'type': 'datetime-local'}),
            }
#In the process of testing datetimeinput field --Review 
#DateTimeInput¶
#class DateTimeInput¶
#input_type: 'text'
#template_name: 'django/forms/widgets/datetime.html'

class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]

class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]