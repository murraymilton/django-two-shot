from django.urls import path
from receipts.views import create_account,all_receipts, create_receipt,category_list, account_list, create_category

urlpatterns = [
    path('', all_receipts, name="home" ),
    path('', all_receipts, name='all_receipts'),
    path('create/', create_receipt, name='create_receipt'),
    path('categories/', category_list, name='category_list'),
    path('accounts/', account_list, name='account_list'),
    path('categories/create/', create_category, name='create_category'),
    path('accounts/create/', create_account, name='create_account'),
]