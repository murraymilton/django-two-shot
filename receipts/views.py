from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, AccountForm, CategoryForm, ExpenseCategoryForm

@login_required
def all_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipts': receipts,
    }
    return render(request, 'receipts/receipt_list.html', context)


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        'form':form
    }

    return render(request, 'receipts/create_receipt.html', context)

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []

    for category in categories:
        receipt_count = Receipt.objects.filter(category=category, purchaser=request.user).count()
        category_data.append({
            'name': category.name,
            'receipt_count': receipt_count,
        })

    context = {
        'categories': category_data,
    }

    return render(request, 'receipts/category_list.html', context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = []

    for account in accounts:
        receipt_count = Receipt.objects.filter(account=account, purchaser=request.user).count()
        account_data.append({
            'name': account.name,
            'number': account.number,
            'receipt_count': receipt_count,
        })

    context = {
        'accounts': account_data,
    }

    return render(request, 'receipts/account_list.html', context)


@login_required
def create_category(request):
    form = ExpenseCategoryForm(request.POST)
    if form.is_valid():
        expense_category = form.save(False)
        expense_category.owner = request.user
        expense_category.save()
        return redirect('category_list')
        pass
    else:
        form = ExpenseCategoryForm()

    context = {
        'form': form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context = {
        "form": form,
    }

    return render(request, 'receipts/create_account.html', context)